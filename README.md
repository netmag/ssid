Server Side Include Daemon
==========================

GPLv2 Copyright (C) Radim Kolar (hsn@sendmail.cz) 1997-9, 2020

Static HTML site generator used in 90's for **N**etwork **C**omputing **I**nfo **C**enter
by NetMag.cz magazine.

### How to run:
   java -jar ssid.jar [config file]

Config file defaults to *ssid.conf*.

### What it does:
 1. SSID takes path to *ssid.conf* as command line argument.
 1. Config file sets various settings and points to Apache *httpd.conf*.
 1. SSID Scans Apache document roots for *.shtml* files,
 1. executes SSI commands inside and generates *.html* files from them.

 This will turn dynamic pages with Apache **S**erver **S**ide **I**ncludes directives [mod_include](https://httpd.apache.org/docs/1.3/mod/mod_include.html)
 into static pages which can be served by any http server capable of serving
 static pages.

### Limitations:
 * Dates (names of months) are hard coded in Czech language
 * Execution of CGI's from .shtml is not supported
 * Needs apache httpd.conf file
