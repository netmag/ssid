import java.io.*;

class alias implements Serializable
{

static final long serialVersionUID = 813755734084986403L;

public String virtual,real;
public boolean execute;

alias (String virt, String real, boolean exec)
{
  this.virtual=virt;
  this.real=real;
  this.execute=exec;
}

public boolean equals(Object o)
{
 if(!(o instanceof alias)) return false;
 return virtual.equals(((alias)o).virtual);
}

}
