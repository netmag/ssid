import java.io.*;
import java.util.*;

class ssid {

private Vector servers;
private long rescandirtime;
private long rescanshtmltime;

private int MinRescanHTMLTime,MaxRescanHTMLTime,RescanHTMLLoadFactor;
private int MinRescanDirectoryTime,MaxRescanDirectoryTime,RescanDirectoryLoadFactor;
private int MinFlagsCheckTime;
private String runflag, dieflag, rescanflag, haltflag;

/* protokolovani */
static public String GenerateLog,ErrorLog;
static public PrintStream gl,el;
static public int verbose;

/* pole pro pluginy */
static public plugin plugins[];
public static Hashtable keywords;

/* chyby a omezeni */
/* netestuje vicenasobne definovani server-root */

public final static String ID="Server Side Includes daemon/0.98 FAAST";

public static void main(String[] args) {

      if(args.length==0) {
         args=new String[1]; /* dirty hack */
         args[0]="ssid.cnf";
      }
      if (! new File(args[0]).canRead()) {
         System.out.println("Can not read config file: "+args[0]);
         throw new IllegalArgumentException();
      }
      ssid server=new ssid();
      try
      {
         server.readSSIDconfig(args[0]);
      }
      catch (FileNotFoundException e) {System.out.println("Config file not found.");return;}
      catch (IOException e) {System.out.println("Error reading config file.");return;}
      catch (IllegalArgumentException e) { return; }

      server.loop();

} /* main */


ssid ()
{
  System.out.println(ID+"\n(c) Radim Kolar (hsn@sendmail.cz) 1997-8, 2020");
  System.out.println("See https://gitlab.com/netmag/ssid for updates.\n");

  ObjectInputStream ois=null;
                        try {
                                ois =
                                    new ObjectInputStream(new FileInputStream("ssid.ser"));
                                System.out.println(new Date()+" Restoring SSID snapshot...");
                                servers = (Vector)ois.readObject();
                        }
                        catch (FileNotFoundException e) { }
                        catch (Throwable e) {
                                System.err.println(e);
                        }
                        finally
                         {
                             if(ois!=null) try{ois.close();}
                                            catch(IOException e) {};
                             ois=null;
                         }

  if(servers==null) servers=new Vector();
   else
    System.out.println(new Date()+" SSID snapshot restored.");

  initSSIDdefaults();
  keywords=new Hashtable();
}

private void save()
{
   try {
        BufferedOutputStream bos = new BufferedOutputStream
              (new FileOutputStream("ssid.ser"));
        ObjectOutputStream oos =
               new ObjectOutputStream(bos);
                                oos.writeObject(servers);
                                oos.close();
                                bos.close();
                        }
                        catch (Throwable e) {
                                System.err.println("error when saving: "+e);
                                return;
                        }


}

private static void insert_plugin(String name)
{
 System.out.print("Inserting plugin "+name);
 System.out.flush();
 plugin p;
 try{
 p=(plugin)Class.forName(name).newInstance();
 }
 catch(Exception e) { System.out.println(" ... failed");return; }
 if(p.getKeywords()==null) { System.out.println(" - no keywords, ignoring");return;}
 if(p.getKeywords().length==0) { System.out.println(" - no keywords, ignoring");return;}

 if(plugins==null) plugins=new plugin[1];
  else
   {
    plugin tmp[]=new plugin[plugins.length+1];
    System.arraycopy(plugins,0,tmp,0,plugins.length);
    plugins=tmp;
   }
 plugins[plugins.length-1]=p;
 System.out.println("/"+p.getName());

 /* pridat obsluhovana klicova slova */
 String kw[]=p.getKeywords();
 for(int i=0;i<kw.length;i++)
  {
   if(keywords.get(kw[i])!=null) {
                                    System.out.println("WARNING: Keyword "+kw[i]+" is allready handled by another plug-in!");
                                    return;
                                 }
  keywords.put(kw[i].toLowerCase(), new Integer(plugins.length-1));
  }

}

/* default VALUES */
private void initSSIDdefaults()
{
 MinRescanHTMLTime=20;
 MaxRescanHTMLTime=60;
 RescanHTMLLoadFactor=25;

 MinRescanDirectoryTime=180;
 MaxRescanDirectoryTime=1500;
 RescanDirectoryLoadFactor=10;

 ErrorLog=GenerateLog=null;
 haltflag=runflag=dieflag=rescanflag=null;
 verbose=1;
 MinFlagsCheckTime=30*1000;
}

private boolean checkFlag(String f)
{
 return checkFlag(f, true);
}

private boolean checkFlag(String f, boolean remove)
{
 if(f==null) return false;
 File fl=new File(f);
 if(fl.canRead() && fl.canWrite())
  if(remove)
    { generate_log("Detected flag "+f);fl.delete();return true;}
  else
   return true;
 return false;
}

private void createFlag(String f)
{
 try
 {
  (new FileOutputStream(f)).close();
 }
 catch (Exception e) {}
}

public void readSSIDconfig(String filename) throws FileNotFoundException, IOException
{
 String line,token;
 StringTokenizer st;

 DataInputStream dis=new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));
 System.out.println("Parsing SSID config file: "+filename);
 while ( (line = dis.readLine()) != null)
 {
          if(line.startsWith("#")) continue;
	  st=new StringTokenizer(line);
          if(st.hasMoreTokens()==false) continue;
          token=st.nextToken();

          if(token.equalsIgnoreCase("MinRescanHTMLTime")) {MinRescanHTMLTime=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("MaxRescanHTMLTime")) {MaxRescanHTMLTime=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("RescanHTMLLoadFactor")) {RescanHTMLLoadFactor=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }

          if(token.equalsIgnoreCase("MinRescanDirectoryTime")) {MinRescanDirectoryTime=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("MaxRescanDirectoryTime")) {MaxRescanDirectoryTime=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("RescanDirectoryLoadFactor")) {RescanDirectoryLoadFactor=Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("MinFlagsCheckTime")) {MinFlagsCheckTime=1000*Integer.valueOf(st.nextToken()).intValue();
          					    continue;
                                                    }
          if(token.equalsIgnoreCase("ApacheConfig")) { apache.readapacheconfig(st.nextToken(),servers);
          					       continue;
                                                    }

          if(token.equalsIgnoreCase("GenerateLog")) { GenerateLog=st.nextToken();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("ErrorLog")) { ErrorLog=st.nextToken();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("VerboseLevel")) { verbose=Integer.valueOf(st.nextToken()).intValue();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("PlugIn")) { insert_plugin(st.nextToken());
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("RunFlag")) { runflag=st.nextToken();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("ShutdownFlag")) { dieflag=st.nextToken();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("RescanFlag")) { rescanflag=st.nextToken();
          					       continue;
                                                    }
          if(token.equalsIgnoreCase("HaltFlag")) { haltflag=st.nextToken();
          					       continue;
                                                    }

          System.out.println("Unknown keyword "+token+" in SSID config. ");
}
 dis.close();
}


public void loop()
{
 long lastscan,lastcheck;
 if(servers.size()==0) { System.out.println("\nFATAL ERROR: No www servers defined in config file. STOP.");
                           return;
                         }

 error_log("Error log opened.");
 generate_log("Generate log opened.");


 generate_log("Performing boot-time full scan...");
 init_timers();

 generate_log("Server init done.");

 rescan_shtml(true);

 lastscan=System.currentTimeMillis();
 lastcheck=System.currentTimeMillis();

 while(true)
 {
 /* pauza */
 if(MinFlagsCheckTime<rescanshtmltime)
     pauzams(MinFlagsCheckTime);
 else
   pauzams(rescanshtmltime);
 /* test na flagy */
 if(checkFlag(dieflag)) break;
 if(checkFlag(rescanflag)) {init_timers();rescan_shtml(true);lastscan=lastcheck=System.currentTimeMillis();continue;}
 createFlag(runflag);
 if(checkFlag(haltflag,false)) continue;
 if(System.currentTimeMillis()>rescandirtime+lastscan)
     {  init_timers();rescan_shtml(true);  lastscan=lastcheck=System.currentTimeMillis();continue;}
 if(System.currentTimeMillis()>rescanshtmltime+lastcheck)
  { rescan_shtml(true); lastcheck=System.currentTimeMillis();continue;}
 }
 save();
}

private void halt()
{
 generate_log("Generator halted");
 while(true)
 {
	pauzams(MinFlagsCheckTime);
        if(!checkFlag(haltflag,false)) break;

 }
 generate_log("Generator continues");
}

private void init_timers()
{
 long now=System.currentTimeMillis();
 long eta;

 /* RESCAN DIR TIME */
 rescan_dir();
 eta=System.currentTimeMillis()-now;

 eta*=RescanDirectoryLoadFactor;
 eta=Math.min(MaxRescanDirectoryTime*1000L,eta);
 eta=Math.max(MinRescanDirectoryTime*1000L,eta);
 rescandirtime=eta;

 /* RESCAN SHTML TIME */

 now=System.currentTimeMillis();
 rescan_shtml(false);
 eta=System.currentTimeMillis()-now;

 eta*=RescanHTMLLoadFactor;
 eta=Math.min(MaxRescanHTMLTime*1000L,eta);
 eta=Math.max(MinRescanHTMLTime*1000L,eta);
 rescanshtmltime=eta;

 if(ssid.verbose>1) System.out.println("Timers recalibrated. dir_scan every "+((int)rescandirtime/1000)+
  "s, shtml_check every "+(int)(rescanshtmltime/1000)+"s");
}

public static void pauza(int sec)
{
	 try
			{
				Thread.sleep(sec*1000);
			}
	 catch (InterruptedException e)
	   { }
}

public static void pauzams(long sec)
{
	 try
			{
				Thread.sleep(sec);
			}
	 catch (InterruptedException e)
	   { }
}

public synchronized void rescan_dir()
{
 wwwserver s;
 for(int i=0;i<servers.size();i++)
  {
    s=(wwwserver)servers.elementAt(i);
    s.rescan_directory();
  }
}

public synchronized void rescan_shtml(boolean generate)
{
 wwwserver s;
 for(int i=0;i<servers.size();i++)
  {
    s=(wwwserver)servers.elementAt(i);
    s.rescan_documents(generate);
  }
}

public static void generate_log(String what)
{
 System.out.println(new Date().toString()+" "+what);
 if(GenerateLog==null) return;

 if(gl==null)
    /* otevrit vystupni soubor */
    {
      try{
      gl=new PrintStream(new BufferedOutputStream(
       new  FileOutputStream(GenerateLog )));
      }
      catch (IOException e)
        {
          GenerateLog=null;
          System.out.println("ERROR: Cannot open logfile: "+GenerateLog+". Running without it.");
          return;
        }

    } /* open logfile */

 gl.println(new Date().toString()+" "+what);

 if(gl.checkError()) {gl.close();gl=null;}

}

public static void error_log(String what)
{
 System.out.println(new Date().toString()+" "+what);

 if(ErrorLog==null) return;
 if(el==null)
    /* otevrit vystupni soubor */
    {
      try{
      el=new PrintStream(new BufferedOutputStream(
       new  FileOutputStream(ErrorLog )));
      }
      catch (IOException e)
        {
          ErrorLog=null;
          System.out.println("ERROR: Cannot open logfile: "+ErrorLog+". Running without it.");
          return;
        }
    } /* open logfile */

 el.println(new Date().toString()+" "+what);

 if(el.checkError()) {el.close();el=null;}
}
}
