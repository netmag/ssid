import java.io.*;
import java.util.*;

public class wwwserver implements Serializable {

static final long serialVersionUID = 2277452205725033546L;

private String name;
private String admin;
public String AccessConfig;
public String ResourceConfig;
private Vector aliases;
private Vector source;

 wwwserver(String s)
 {
  this.name=s;
  admin=null;
  aliases=new Vector();
  source=new Vector(50,5);
 }

public void setName(String s)
 {
  this.name=s;
 }

public String getName()
 {
  return name;
 }

 public boolean equals(Object o)
 {
  if(!(o instanceof wwwserver)) return false;
  wwwserver w=(wwwserver)o;
  return name.equals(w.getName());
 }

public void setAdmin(String s)
 {
  this.admin=s;
 }
public String getAdmin()
 {
  if(admin==null) admin="webmaster@"+name;
  return admin;
 }

public void addalias(String virt, String real, boolean exec)
 {
  real=noendingslash(real);
  alias a=new alias(virt,real,exec);
  if(aliases.contains(a)) return;
  int si=virt.length();
  int i;
  File root=new File(real);
  if(root.isDirectory()==false) return;

  for(i=0;i<aliases.size();i++)
  {
   if(si< ((alias)aliases.elementAt(i)).virtual.length() ) break;
  }
  aliases.insertElementAt(a,i);
/*  System.out.println("Alias "+virt+" = "+real); */
/*  dirlister(real); */
}

/* *** source/ target pool management *** */

public depend findDepend(String fn)
{
 /* krok c.1 - najit zda mame fn jiz v sources */
 depend d;
 for(int i=0;i<source.size();i++)
  {
   d=(depend)source.elementAt(i);
   if(d.fullname.equals(fn)) { return d;}
  }
  return null;
}

public void addSource(String fn,shtml target)
{
  depend d=findDepend(fn);

  if(d==null) { d=new depend(fn);source.addElement(d);}
  if(!d.targets.contains(target)) d.targets.addElement(target);
}

/* nemaze prazdne source objekty! */
public void delSources(shtml target)
{
 /* krok c.1 - najit zda mame fn jiz v sources */
 depend d;
 for(int i=0;i<source.size();i++)
  {
   d=(depend)source.elementAt(i);
   if(d.targets.contains(target)) { d.targets.removeElement(target);}
  }
}

public long latestSource(shtml target)
{
 /* krok c.1 - najit zda mame fn jiz v sources */
 depend d;
 long latest=-1;
 for(int i=0;i<source.size();i++)
  {
   d=(depend)source.elementAt(i);
   if(d.lastmod>latest && d.targets.contains(target) ) { latest=d.lastmod;}
//   System.out.println("Targets dump:"+d.targets);
  }
//  System.out.println("Latest time: "+latest);
  return latest;
}

/* ****** SCANNER *****  */

/* proscanuje vsechny adresare a vytvori shtml objekty */
public void rescan_directory()
{
  long starttime=System.currentTimeMillis();
  if(ssid.verbose>1)System.out.println("Scanning "+name+" for new .shtml files ...");
  alias a;
  int old=source.size();

  Hashtable documents=new Hashtable(200,0.9f);
  depend d;
  shtml document;
  for(int i=0;i<source.size();i++)
   {
    d=(depend)source.elementAt(i);
    for(int j=0;j<d.targets.size();j++)
          {
           document=(shtml)d.targets.elementAt(j);
           documents.put(document.getName(),document);
          }
   }
//   System.out.println("Documents build: size="+documents.size());

  for(int i=0;i<aliases.size();i++)
   {
    a=(alias)aliases.elementAt(i);
    dirlister(a.real,documents);
   }

  /* CLEAN UP DELETED OBJECTS */
  Enumeration docs=documents.elements();
  while(docs.hasMoreElements())
  {
   document=(shtml)docs.nextElement(); // documents.elementAt(i);
   File f;

   /* test na .shtml */
    f=new File(document.getName());
    /* nezmizel nam SHTML fajl? */
    if(!f.exists()) { ssid.generate_log("[LOST] File "+f+" does not longer exists");
                      f=new File(document.generatename());
                      if(!f.delete() && f.exists()) ssid.error_log("[PERM] Error deleting "+document.generatename());
                      delSources(document);
                      continue;
                    }

    if(!f.canRead()) {
                      ssid.error_log("[LOST PERM] Can not read "+name);
                      ssid.generate_log("[LOST PERM] File "+f+" (can not read)");
                      f=new File(document.generatename());
                      if(!f.delete()) ssid.error_log("[PERM] Error deleting "+document.generatename());
                      delSources(document);
                      continue; }

   f=new File(document.generatename());
   /* test .html */
   if (f.exists()==false) { document.generate_html();continue;}
   if (f.canRead()==false) { ssid.error_log("[LOST PERM] Can not read file "+document.generatename());
                           if(f.delete()) ssid.generate_log("[DELE] File "+document.generatename()+" deleted.");
                              else
                               ssid.error_log("[PERM] Can not delete "+document.generatename());
                            continue;}

  }
  /* odstraneni prazdnych sources */
  for(int i=0;i<source.size();i++)
  {
   d=(depend)source.elementAt(i);
   if(d.targets.size()==0) { source.removeElementAt(i);i--;continue;}
  }
  /* ******* clean end *********** */
  if ((ssid.verbose>1)||((ssid.verbose==1)&&
  (source.size()-old>0)))
   System.out.println("End of scan. Stats: "+(source.size()-old)+" new, "+
      source.size()+" total, "+
     "Eta="+ ((int)(System.currentTimeMillis()-starttime)/1000 ) +" sec.");
}

/* zjisti, zda neni potreba znovu vygenerovat html dokument */
public void rescan_documents(boolean generate)
{
 shtml document;
 String name;
 depend d;
 Vector queue;
 File f;

 queue=new Vector(40,5);
  for(int i=0;i<source.size();i++)
   {
    d=(depend)source.elementAt(i);
    f=new File(d.fullname);
    if(d.lastmod!=f.lastModified())
       {
         d.lastmod=f.lastModified();
         for(int j=0;j<d.targets.size();j++)
          {
           document=(shtml)d.targets.elementAt(j);
           if(!queue.contains(document)) queue.addElement(document);
          }
       }
   }

   /* process Queue */
   for(int i=0;i<queue.size();i++)
   {
    document=(shtml)queue.elementAt(i);
    document.generate_html();

   }

}



/* proscanuje rekurzivne adresar a vytvori shtml objekty, pokud neexistuji */

 private void dirlister(String start,Hashtable documents)
 {
  File root=new File(start);
  String filez[];

  if(root.exists()==false) {ssid.error_log("[DIRSCAN] "+start+" is not exists");
                            return;
                           }

  if(root.isDirectory()==false) {ssid.error_log("[ERR] "+start+" is not directory");
                                 return;
                                }
  if(root.canRead()==false) {ssid.error_log("[ERR] Can not read directory "+start);
                             return;
                                }

  ssid.pauzams(5);

//  System.out.println("Scanning directory "+start);
  filez=root.list();
  for(int i=0;i<filez.length;i++)
   {
    File f=new File(root,filez[i]);
    if(f.isDirectory()==true) {dirlister(start+File.separator+filez[i],documents);
                               continue;}
    if(!f.isFile()) continue;
    if(!f.canRead())  {
                         ssid.error_log("[PERM] Can not read "+f.getAbsolutePath());
                         continue;
                      }
    String fpath;
    fpath=f.getAbsolutePath();
    if (filez[i].toLowerCase().indexOf(".sht")>0 && !documents.containsKey(fpath) /*null==findDepend(f.getAbsolutePath())*/)
       {
        shtml sh=new shtml(this,fpath);
        System.out.println("New document "+fpath);
        documents.put(fpath,sh);
       }
   }
 }

String sub_req_lookup_file (String file, String oldrequest)
{
 File f=new File(oldrequest);

 f=new File(f.getParent(),file);
 if(f.canRead()==true) return f.getAbsolutePath();
 return null;
}

String file_to_uri (String file)
{
 int i;
 String uri;
 alias a;
 for(i=aliases.size()-1;i>=0;i--)
 {
   a=(alias)aliases.elementAt(i);
   if(file.indexOf(a.real)==0)
                              {
                               uri=util.odecti(file,a.real);
                               uri=a.virtual+uri;
                               uri=util.no2slash(uri);
                               return uri;
                              }
 }

 System.out.println("Error: Can not convert file "+file+" to uri.");
 System.out.print("  Tried aliased pathes: ");
 for(i=aliases.size()-1;i>=0;i--)
 {
   a=(alias)aliases.elementAt(i);
   System.out.print(a.real);
   System.out.print(" ");
 }
 System.out.println("");

 return null;
}


String sub_req_lookup_uri (String file, String oldfile)
{
 String uri;
 alias a;
 int i;
 // System.out.println("Searching URI="+file);
 if(file.charAt(0)=='/') {
                           uri=util.getparents(file);
    //                     System.out.println("parsed as URI="+uri);

                           for(i=aliases.size()-1;i>=0;i--)
                           {
                               a=(alias)aliases.elementAt(i);
                               if(uri.indexOf(a.virtual)==0)
                              {
                               uri=util.odecti(uri,a.virtual);
                               uri=a.real+File.separator+uri;
                               uri=util.no2slash(uri);
                               File f=new File(uri);
                               if(f.canRead()==false) return null;
                               return uri;
                              }
                           }
                           return null;
                         };
 uri=file_to_uri(oldfile);
 if(uri==null) return null;

 String udir=util.make_dirstr(uri,util.count_dirs(uri));
 return sub_req_lookup_uri(util.no2slash(udir+file),null);
}

String getDocumentRoot()
{
 return ((alias)aliases.elementAt(0)).real;
}

public static String noendingslash(String in)
{
 if(in.lastIndexOf("/")==in.length()-1) return in.substring(0,in.length()-1);
 if(in.lastIndexOf("\\")==in.length()-1) return in.substring(0,in.length()-1);
 return in;
}
}
