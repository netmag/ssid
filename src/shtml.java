import java.util.*;
import java.io.*;

class shtml implements Serializable
{
 static final long serialVersionUID = 8718636161196051078L;

 private String fullpath;
 private transient Stack loopdetection;
 private transient Hashtable variables;

 private wwwserver parent;
 private transient boolean anyerror;

 private transient DataOutputStream ps;

 public static final String START="<!--#";
 public static final String STOP="-->";

 public String getName()
{
 return this.fullpath;
}

public wwwserver getWWWServer()
{
 return parent;
}

public void writeToDocument(String s)
{
 if(ps!=null) try { ps.writeBytes(s);}
              catch (IOException e)
               {ps=null;}
}

 public shtml(wwwserver w,String f)
 {
 this.fullpath=f;
 this.parent=w;
 init_depends();
 long latest=parent.latestSource(this);
 File f1=new File(generatename());
 if(f1.lastModified()<latest) generate_html();
}

private void init_variables()
{
   loopdetection=new Stack();

   variables=new Hashtable();
   variables.put("timefmt","%A, %d-%b-%y %T %Z");
   variables.put("errmsg","[an error occurred while processing this directive]");
   variables.put("sizefmt","abbrev");
   variables.put("PATH","/disabled/for/security/reasons/");
   variables.put("SERVER_SOFTWARE",ssid.ID);
   variables.put("SERVER_NAME",parent.getName());
   variables.put("SERVER_PORT","80");
   variables.put("REMOTE_HOST","localhost");
   variables.put("REMOTE_ADDR","127.0.0.1");
   variables.put("SERVER_ADMIN",parent.getAdmin());
   variables.put("REMOTE_PORT","1029");
   variables.put("GATEWAY_INTERFACE","CGI/1.1");
   variables.put("SERVER_PROTOCOL","HTTP/1.0");
   variables.put("REQUEST_METHOD","GET");
   variables.put("QUERY_STRING","");
   variables.put("USER_NAME","root");
   variables.put("DOCUMENT_ROOT",parent.getDocumentRoot());
   variables.put("SCRIPT_FILENAME",this.fullpath);
   variables.put("SCRIPT_NAME",parent.file_to_uri(fullpath));
   variables.put("DOCUMENT_URI",parent.file_to_uri(fullpath));
   variables.put("DOCUMENT_PATH_INFO","");
   variables.put("DOCUMENT_NAME",util.get_filename(fullpath));
/*
LAST_MODIFIED=Monday, 29-Dec-97 13:39:40 MET
*/
 initdate();
}

private void init_plugins()
{
 if(ssid.plugins==null) return;
 for(int i=0;i<ssid.plugins.length;i++)
  ssid.plugins[i].newDocument(this);
}

private void initdate()
{
 String format=(String)variables.get("timefmt");
 Date d=new Date();
 long mil;
 variables.put("DATE_LOCAL",util.strftime(d,format));
 mil=d.getTime();
 mil+=(d.getTimezoneOffset()-1)*60*1000;
 d=new Date(mil);
 variables.put("DATE_GMT",util.strftime(d,format));
}

/* zjisti,  zda je potreba znovu *.html vygenerovat */
public String generatename()
{
 int i=fullpath.lastIndexOf(".");
 return fullpath.substring(0,i)+".html";
}

private void init_depends()
{
  /* inicializace pole */

  try{  init_variables(); init_plugins();ps=null;anyerror=false;processfile(fullpath);
         } catch (Exception e) {e.printStackTrace();}
     finally
     { drop_variables();}

}

void generate_html()
{
 File gen=new File(generatename());
 long starttime=System.currentTimeMillis();
 ps=null;
 init_variables();
 init_plugins();
 if(ssid.verbose>1) {System.out.print("Generating "+gen);
                     System.out.flush();}
 anyerror=false;
 try {
       ps=new DataOutputStream(new BufferedOutputStream(new
       FileOutputStream(gen ),4096));
       parent.delSources(this);
       //  dependson=new Vector();
       processfile(fullpath);
       ps.close();
       ps=null;
       if(ssid.verbose>1) System.out.println("  ok. Eta="+ ((int)(System.currentTimeMillis()-starttime)/1000 ) +" sec.");
       ssid.generate_log((anyerror? "[ERR]" : "[OK]")+" File "+gen+" generated "+ " in "+
       ((int)(System.currentTimeMillis()-starttime)/1000 ) +" sec.");
 }
 catch (Exception e) {System.out.println("  error!");
                      e.printStackTrace();if (ps!=null) try {ps.close();}
                                                        catch(IOException ee) {}
                      gen.delete();
                      ssid.generate_log("[FATAL] File "+gen+" NOT GENERATED!");
                      ps=null;
                      }
 finally{
 drop_variables();
 }
}

private void drop_variables()
{
 variables=null;
 loopdetection=null;

 /* deaktivovat pluginy - IMHO neni nutne */

}

public void add_depend(String s)
{
 if(s==null) return;
 parent.addSource(s,this);
}

public void processfile(String fn) throws IOException
{
  String line;
  add_depend(fn);

  if(loopdetection.search(fn)>-1)
             { error("Recursive include of "+fn);return;}

  FileInputStream fis=new FileInputStream(fn);
  DataInputStream dis=new DataInputStream(new BufferedInputStream(fis,4096));

  loopdetection.push(new String(fn));

 while ( find_string(dis,START,ps) )
 {
   line=get_directive(dis);
// System.out.println(line);
   if(line.equals("include")) { handle_include(dis,fn,true); continue;}
   if(line.equals("flastmod")) { handle_flastmod(dis,fn); continue;}
   if(line.equals("printenv")) { handle_printenv(dis,fn); continue;}
   if(line.equals("set")) { handle_set(dis,fn); continue;}
   if(line.equals("echo")) { handle_echo(dis,fn); continue;}
   if(line.equals("config")) { handle_config(dis,fn); continue;}
   if(line.equals("fsize")) { handle_fsize(dis,fn); continue;}

   /* prohledame jeste pluginy */
   Integer I;
   I=(Integer)ssid.keywords.get(line);
   if(I!=null) { ssid.plugins[I.intValue()].handle_command(line,dis,fn);
                 continue;
               }

   error("Unknown element "+line+" in file "+fn);
//   System.out.println("Directive: "+line);
//   System.out.println("tag: "+get_tag_name(dis));
//   line= get_tag_value(dis);
//   System.out.println("value: "+line);
//   System.out.println("Parsed value: "+parse_string(line, true));
   find_string(dis,STOP,null);
 }
  dis.close();
  fis.close();
  // System.out.println(fn+"Closed!");
  loopdetection.pop();
}

/* --------- E r r o r   h a n d l e r ---------- */

public void error(String what)
{
 anyerror=true; /* neco se posralo ... */

 /* Message for BFUs */
 writeToDocument((String)variables.get("errmsg"));

 /* a zaprotokolovat */
 ssid.error_log(what);
}

/* --------- E v e n t s     h a n d l e r s ----------- */


/* todo - security ? */

private int handle_include(DataInputStream in, String r, boolean noexec)  throws IOException
{
    String tag;
    String parsed_string;
    String tag_val;

    while(true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;

        if(tag.equals("file") || tag.equals("virtual")) {
            String rr;
            tag_val=get_tag_value(in);
            parsed_string=tag_val.trim();

	    if (tag.equals("file"))
	    { /* be safe; only files in this directory or below allowed */
                /* nemame radi Bylla */
                parsed_string=util.no2slash(parsed_string);
                tag_val="/"+parsed_string+"/";
		if (parsed_string.charAt(0) == '/' || tag_val.lastIndexOf("/../") >= 0
                || parsed_string.lastIndexOf(":") >= 0
                )
		    { error("unable to include file "+parsed_string+" in parsed file "+r+" (Security Error).");continue;}
		else
		    rr = parent.sub_req_lookup_file (parsed_string, r);
	    } else
		rr = parent.sub_req_lookup_uri (parsed_string, r);

	    if (rr==null)
	        { error("unable to include "+parsed_string+" in parsed file "+r);
                  continue;
                }

                if(loopdetection.search(new String(rr))>-1)
                      { error("Recursive include of "+rr+" in parsed file "+r);continue;}

	    processfile(rr);
        }
        else if(tag.equals("done"))
            return 0;
        else {
            error("unknown parameter "+tag+" to tag include in "+r);
        }
    }
}

private int handle_config(DataInputStream in, String fn) throws IOException {
    String tag, tag_val, parsed_string;

    while(true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;

        if(tag.equals("errmsg")) {
            tag_val=get_tag_value(in);
            if(tag_val==null) return 1;
            parsed_string=parse_string(tag_val,false);
            variables.put("errmsg",parsed_string);
        } else if(tag.equals("timefmt")) {
            tag_val=get_tag_value(in);
            if(tag_val==null) return 1;
            parsed_string=parse_string(tag_val,false);
            variables.put("timefmt",parsed_string);
            initdate();
        }
        else if(tag.equals("sizefmt")) {
            tag_val=get_tag_value(in);
            if(tag_val==null) return 1;
            parsed_string=parse_string(tag_val,false);

	    // 	decodehtml(parsed_string);
            if(parsed_string.equals("bytes") || parsed_string.equals("abbrev"))
                   variables.put("sizefmt",parsed_string);
              else
                   error("bad value "+parsed_string+" for attribute sizefmt (config directive) in file "+fn);

        }
        else if(tag.equals("done"))
            return 0;
        else {
            error("unknown attribute "+tag+" to tag config in file "+fn);
        }
    }
}


private int handle_printenv(DataInputStream in,String s )  throws IOException
{
    String tag;
    int i;

    tag=get_tag_name(in);

    if(tag==null)  return 1;

    else if(tag.equals("done")) {
                 Enumeration vals=this.variables.elements();
                 Enumeration vars=this.variables.keys();
                 for(;vars.hasMoreElements();)
                 {
                  writeToDocument(vars.nextElement()+"="+vals.nextElement()+"\n");
                 }
                 return 0;
    } else {
        error("printenv directive does not take tags");
        return -1;
    }
}


private int handle_set(DataInputStream in, String s) throws IOException
{
    String tag;
    String parsed_string;
    String tag_val;
    String var;

    var=null;
    while (true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;
        if(tag.equals("done")) return 0;
        tag_val=get_tag_value(in);
        if(tag_val==null)  return 1;
        else if (tag.equals("var")) {
            var = tag_val;
        } else if (tag.equals("value")) {
            if (var == null) {
                error("variable must precede value in set directive");
                return -1;
            }
            parsed_string=parse_string(tag_val, false);
            variables.put(var, parsed_string);
        }
    }
}


private int handle_echo (DataInputStream in, String ss) throws IOException {
    String tag, tag_val;

    while(true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;
        if(tag.equals("done")) return 0;
        tag_val=get_tag_value(in);
        if(tag_val == null ) return 1;

        if(tag.equals("var")) {
            String s=(String)variables.get(tag_val);
	    if (s!=null) writeToDocument(s);
                   	    else writeToDocument("(none)");
        }
        else {
            error("unknown parameter "+tag+" to tag echo");
        }
    }
}

private int handle_flastmod(DataInputStream in,String r) throws IOException {


    String tag;
    String tag_val;
    String fn;

    while(true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;
        if(tag.equals("done")) return 0;
        tag_val=get_tag_value(in);
        if(tag_val == null ) return 1;
        else

            if(null!=(fn=find_file(r,tag,tag_val)))
              {
               File f=new File(fn);
               Date d=new Date(f.lastModified());
               writeToDocument(util.strftime(d,(String)variables.get("timefmt")));
              }
            else
             error("Can't get last modified date for "+tag_val);

      } /* while */
}

private int handle_fsize(DataInputStream in,String r) throws IOException {


    String tag;
    String tag_val;
    String fn;

    while(true) {
        tag=get_tag_name(in);
        if(tag==null) return 1;
        if(tag.equals("done")) return 0;
        tag_val=get_tag_value(in);
        if(tag_val == null ) return 1;
        else

            if(null!=(fn=find_file(r,tag,tag_val)))
              {
               File f=new File(fn);
               long l=f.length();

               if(variables.get("sizefmt").equals("bytes"))
                  writeToDocument(l+""); /* TODO: dodelat carky mezi tim?? */
               else
               {
                 if(l==0) writeToDocument("   0k");
                 else if(l < 1024) writeToDocument("   1k");
                 else if(l < 1024*10-512)
                       writeToDocument("   "+(l + 512) / 1024+"k");
                 else if(l < 1024*100-512)
                       writeToDocument("  "+(l + 512) / 1024+"k");
                 else if(l < 1024*1000-512)
                       writeToDocument(" "+(l + 512) / 1024+"k");
                 else if(l < 1024*10000-512)
                       writeToDocument(""+(l + 512) / 1024+"k");
                 else if(l < 1024*100000)
                       writeToDocument(""+(l*10 / 1048576)/10f+"M");
                 else  if(l <1024*1000000)
                       writeToDocument(" "+(l / 1048576)+"M");
                 else
     	               writeToDocument(""+ l / 1048576+"M");
               }/* else sizefmt */

               // writeToDocument(util.strftime(d,(String)variables.get("timefmt")));
              }
            else
             error("Can't get file size for "+tag_val);

      } /* while */
}

/* ------- Pomocne Procedury, ported from apache ------- */

static private boolean find_string(DataInputStream dis, String str, DataOutputStream ps) throws IOException
{
 int l=str.length();
 int p,x;
 char c;

 p=0;
 while(true)
 {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return false; }

        if(c == str.charAt(p)) {
            if((++p) == l)
                return true;
        }
        else {
            if(ps!=null){
                if(p>0)   {
                    for(x=0;x<p;x++) {
                        ps.writeByte(str.charAt(x));
                                     }
                          }
                ps.writeByte(c);
                        }
            p=0;
        }
    }

}

static private String get_directive(DataInputStream dis) throws IOException
{
 char c;
 String d="";
    /* skip initial whitespace */
    while(true) {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return ""; }

        if(!Character.isSpace(c))
            break;
    }
    /* now get directive */
    while(true) {
        d+= Character.toLowerCase(c);
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return ""; }

        if(Character.isSpace(c))
            break;
    }
    return d;
}

static public String get_tag_name(DataInputStream dis) throws IOException
{
    char c;
    String t="";

    /* skip initial whitespace */
    while(true) {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return null; }

        if(!Character.isSpace(c))
            break;
    }

    /* tags can't start with - */
    if(c == '-') {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return null; }

        if(c == '-') {
            do {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return null; }

	    } while (Character.isSpace(c));
            if(c == '>') return "done";

        }
	return null; /* "";  failed */
    }

    /* find end of tag name */
    while(true) {
	if(c == '=' || Character.isSpace(c)) break;
	t+= Character.toLowerCase(c);
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return null; }

    }


    /* mezera za jmenem tagu */
    while (Character.isSpace(c))
    {
        try {
        c=(char)dis.readByte();
        }
        catch (EOFException e) { return null; }
    }

    if (c != '=') return null; /* ""; */

 return t;
}

public String get_tag_value(DataInputStream dis) throws IOException
{
 String t="";
 char c,term;

/* space after = */

    do {
        try {
        c=(char)dis.readByte();

        }
        catch (EOFException e) { return null; }
    } while (Character.isSpace(c));

    /* we should allow a 'name' as a value */

    if (c != '"' && c != '\'') return "";
    term = c;
    while(true) {
        try { c=(char)dis.readByte(); }
        catch (EOFException e) { return null; }

/* Want to accept \" as a valid character within a string. */
	if (c == '\\') {
	    t+= c; /* Add backslash */
        try { c=(char)dis.readByte(); }
        catch (EOFException e) { return null; }

	    if (c == term) /* Only if */
		t=t.substring(0,t.length()-1); /* Replace backslash ONLY for terminator */
	} else if (c == term) break;
	t+=c;
    }
//    if (dodecode) decodehtml(tag_val);

 return parse_string(t,false);
}

/* Do variable substitution on strings */

private String parse_string(String sin, boolean leave_name)
{
    char ch;
    char in;
    String next = "";
    int stringpos=0;

//  System.out.println("Parse_string: "+sin);

    while (stringpos<sin.length()) {
        ch = sin.charAt(stringpos++);
        switch(ch) {
          case '\\':
            if (stringpos==sin.length()) { next+=ch;return next;}
            in=sin.charAt(stringpos);

	    if(in == '$')
		{next+=in;stringpos++;}
	    else
		next+=ch;
            break;
          case '$':
          {
            String var="";
            String vtext="";
            String val="";
            int braces=0;
            /*
             * Keep the $ and { around because we do no substitution
             * if the variable isn't found
             */
            vtext+=ch;
            if(stringpos<sin.length()) in=sin.charAt(stringpos);
             else in='$';

            if (in == '{') { braces = 1; vtext+= in; stringpos++; }
            while (stringpos<sin.length()) {
                in = sin.charAt(stringpos++);

                if (braces == 1) {  if (in == '}') break; }
                else if (! (Character.isLetterOrDigit(in) || (in == '_') ) ) break;
                vtext+=in;
                var+=in;
            }
            if (braces == 1) {
                if (in != '}') {
                    return "";
                }  else
                    stringpos++;
            }
  //        stringpos--;
            val=(String)variables.get(var);
            if( (val==null) && (leave_name) ) val=vtext;
            if (val!=null) next+=val;

            break;
          }
          default:
            next+= ch;
            break;
        }
    }
//  System.out.println("Parsed_string: "+next);
    return next;
}


public String find_file(String r, String tag, String tag_val)
{
 String directive;
 String z;
// System.out.println("find file: "+r+" "+tag+" "+tag_val);
    if(tag.equals("file")) {
        tag_val=util.no2slash(tag_val); /* get rid of any nasties */
        directive="/"+tag_val+"/";
		if (tag_val.charAt(0) == '/' || directive.lastIndexOf("/../") >= 0
                || directive.lastIndexOf(":") >= 0
                   ) { error("Security error: Can't process file "+tag_val+" in file "+r); return null; }

		else
		    { z=parent.sub_req_lookup_file (tag_val, r);
                      add_depend(z);
                      return z;
                    }

        }
    else if(tag.equals("virtual")) { z=parent.sub_req_lookup_uri (tag_val, r);
                                     add_depend(z);
                                     return z;
                                   }
    else  return null;
}

public boolean equals(Object o)
{
 if(!(o instanceof shtml)) return false;
 shtml tmp=(shtml)o;
 return this.fullpath.equals(tmp.getName());
}

public String toString()
{
 return fullpath;
}

}
