import java.util.*;

public class util {

public static String getparents(String string)
{
    int l, w;
    char name[];
    
    name=new char[string.length()+1];
    string.getChars(0,string.length(),name,0);
    name[string.length()]='\0';

    /* Four paseses, as per RFC 1808 */
    /* a) remove ./ path segments */

    for (l=0, w=0; name[l] != '\0';)
    {
	if (name[l] == '.' && name[l+1] == '/' && (l == 0 || name[l-1] == '/'))
	    l += 2;
	else
	    name[w++] = name[l++];
    }

    /* b) remove trailing . path, segment */
    if (w == 1 && name[0] == '.') w--;
    else if (w > 1 && name[w-1] == '.' && name[w-2] == '/') w--;
    name[w] = '\0';

    /* c) remove all xx/../ segments. (including leading ../ and /../) */
    l = 0;

    while(name[l]!='\0') {
        if(name[l] == '.' && name[l+1] == '.' && name[l+2] == '/' &&
	    (l == 0 || name[l-1] == '/')) {
		int m=l+3,n;

		l=l-2;
		if(l>=0) {
		    while(l >= 0 && name[l] != '/') l--;
		    l++;
		}
		else l=0;
		n=l;
		while((name[n]=name[m])>0) {++n;++m;}
            }
	else ++l;
    }

    /* d) remove trailing xx/.. segment. */
    if (l == 2 && name[0] == '.' && name[1] == '.') name[0] = '\0';
    else if (l > 2 && name[l-1] == '.' && name[l-2] == '.' && name[l-3] == '/')
    {
	l = l - 4;
	if (l >= 0)
	{
	    while (l >= 0 && name[l] != '/') l--;
	    l++;
	}
	else l = 0;
	name[l] = '\0';
    }
    return new String(name,0,l);
}


public static String make_full_path(String src1, String src2) {
    int x;

    x = src1.length();
    if (x == 0) return "/"+src2;

    if (src1.charAt(x - 1) != '/') return src1+"/"+src2;
    else return src1+src2;
}

public static String make_dirstr(String s, int n) {
    int x,f;
    String res;

    for(x=0,f=0;x<s.length();x++) {
        if(s.charAt(x) == '/')
            if((++f) == n) {
		res=new String(s.substring(0,x));
                return res+"/";
            }
    }

    if (s.charAt(s.length() ) == '/')
        return s;
    else
        return  s+"/";
}

public static int count_dirs(String path) {
    int x,n;

    for(x=0,n=0;x<path.length();x++)
        if(path.charAt(x) == '/') n++;
    return n;
}


public static int unescape_url(String ur) {
    int x,y;
    boolean badesc, badpath;

    char url[];

    url=new char[ur.length()+1];
    ur.getChars(0,ur.length(),url,0);
    url[ur.length()]='\0';

    badesc = false;
    badpath = false;
    for(x=0,y=0;url[y]>0;++x,++y) {
	if (url[y] != '%') url[x] = url[y];
	else
	{
	    if ((-1==Character.digit(url[y+1],16)) || -1==Character.digit(url[y+2],16))
	    {
		badesc = true;
		url[x] = '%';
	    } else
	    {
		url[x] = x2c(new String(url,y+1,2));
		y += 2;
		if (url[x] == '/' || url[x] == '\0') badpath = true;
	    }
        }
    }
    url[x] = '\0';
    if (badesc) return 2;
    else if (badpath) return 1;
    else return 0;
}


public static char x2c(String wh) {
    char what[];
    what =new char[2];

    wh.getChars(0,wh.length() < 2 ? wh.length() : 2,what,0);
    char digit;

    digit =(char) ((what[0] >= 'A') ? ((what[0] & 0xdf) - 'A')+10 : (what[0] - '0'));
    digit *= 16;
    digit += (what[1] >= 'A' ? ((what[1] & 0xdf) - 'A')+10 : (what[1] - '0'));
    return(digit);
}

/* ZATIM NEJSOU PODPOROVANY TYTO TAGY:
%j - den v roce
%U - tyden (nedele)
%W - tyden (pondeli)
*/
public static String strftime(Date d,String format)
{
 String res="";
 char ch;

 String locabbday[]={ "Ne","Po","Ut","St","Ct","Pa","So" };
 String locday[]={ "Nedele","Pondeli","Utery","Streda","Ctvrtek","Patek","Sobota" };
 String locabbmonth[]={ "Led","Uno","Brez","Dub","Kvet","Cer","Cnc","Srp","Zar","Rij","Lis","Pros" };
 String locmonth[]={ "Ledna","Unora","Brezna","Dubna","Kvetna","Cervna","Cervence","Srpena","Zari","Rijna","Listopadu","Prosince" };

 for(int i=0;i<format.length();i++)
 {
   ch = format.charAt(i);
   if(ch!='%') {res+=ch; continue; }
   if (i==format.length()-1) continue;
   ch = format.charAt(++i);
   switch(ch) {
     case '%': res+="%";break;
     case 'a': res+=locabbday[d.getDay()];break;
     case 'A': res+=locday[d.getDay()];break;
     case 'b': res+=locabbmonth[d.getMonth()];break;
     case 'B': res+=locmonth[d.getMonth()];break;
     case 'c':
              format=format.substring(0,i+1)+"%x %X"+format.substring(i+1,format.length());
              break;
     case 'd': res+=d.getDate()>9? ""+d.getDate() : "0"+d.getDate();break;
     case 'D':
              format=format.substring(0,i+1)+"%m/%d/%y"+format.substring(i+1,format.length());
              break;


     case 'e': res+=d.getDate()>9 ? new Integer(d.getDate()).toString() : " "+d.getDate();break;
     case 'h': res+=locabbmonth[d.getMonth()];break;
     case 'H': if (d.getHours()==0) { res+="00";break;}
               res+=d.getHours()>9 ? ""+d.getHours(): "0"+d.getHours();break;
     case 'I':{
                int j=d.getHours() % 12;
                if (j==0) i=12;

                res+=j>9? ""+j:"0"+j;
                break;
              }
//    case 'j': res+=d.getDays();break; 0-365 den v roce - zatim nevedeme
    case 'm': res+=d.getMonth()>8? ""+(1+d.getMonth()) : "0"+(1+d.getMonth());break;
    case 'M': if (d.getMinutes()==0) { res+="00";break;}
              res+=d.getMinutes()>9? d.getMinutes()+"": "0"+d.getMinutes();break;
    case 'n': res+="\n";break;
    case 'p': if(d.getHours()<12) res+="dopoledne";
                             else res+="odpoledne";break;
              /* AM/ PM nevedeme - NEMAM V TOMHLETOM MOC JASNO, BOHUZEL ...*/
    case 'r': /*           */
              format=format.substring(0,i+1)+"%I:%M:%S %p"+format.substring(i+1,format.length());
              break;


    case 'S': if(d.getSeconds()==0) {res+="00";break;}
              res+=d.getSeconds()>9?d.getSeconds()+"": "0"+d.getSeconds();break;
    case 't': res+="\t";break;
    case 'T': format=format.substring(0,i+1)+"%H:%M:%S"+format.substring(i+1,format.length());
              break;
    case 'U': break;
    case 'w': res+=d.getDay();break;
    case 'x':
              format=format.substring(0,i+1)+"%d. %B %Y%"+format.substring(i+1,format.length());
              break;
    case 'X':
              format=format.substring(0,i+1)+"%T"+format.substring(i+1,format.length());
              break;
    case 'Y': res+=(1900+d.getYear());break;
    case 'y':
              res+=d.getYear();break;
    default: break;
   }

 }
 return res;

}

public static String no2slash(String ur) {
    int x,y;
    char name[];
    ur=ur.replace('\\','/');
    name=new char[ur.length()+1];
    ur.getChars(0,ur.length(),name,0);
    name[ur.length()]='\0';

    for(x=0; name[x]>0;)
        if(x>0 && (name[x-1] == '/') && (name[x] == '/'))
            for(y=x+1;name[y-1]>0;y++)
                name[y-1] = name[y];
	else x++;
   return new String(name,0,x);
}

/* hlavni XXXXXXXccc */
/* maly XXXX */

public static String odecti(String hlavni, String maly)
{
 if(hlavni.indexOf(maly)!=0) return hlavni;
 return hlavni.substring(maly.length(),hlavni.length());
}

public static String get_filename(String fname)
{
 fname=no2slash(fname);
 if(fname.lastIndexOf("/")==-1) return fname; /* zadne / nemame */
 return fname.substring(fname.lastIndexOf("/"),fname.length());
}

public static int count_updirs(String fname)
{
 int n=0;
 int s=0;
 while( (s=1+fname.indexOf("../",s))>0   )
  n++;
 return n;
}


}
