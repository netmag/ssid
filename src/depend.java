import java.io.*;
import java.util.*;


class depend implements Serializable {

static final long serialVersionUID = -8686543597157221283L;

public long lastmod;
public String fullname;
public Vector targets;

depend(String fullname)
{
 this.fullname=fullname;
 File f=new File(fullname);
 this.lastmod=f.lastModified();
 targets=new Vector();
}

public boolean equals(Object o)
{
 if(!(o instanceof depend)) return false;
 return fullname.equals(((depend)o).fullname) ;
}

public String toString()
{
 return fullname+"/"+lastmod;
}
}
