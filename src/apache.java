import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.util.StringTokenizer;
import java.util.Vector;

/**
   Apache httpd.conf reading
*/
class apache {
   public String ServerRoot;

   public apache(String root) {
      this.ServerRoot = root;
   }

   /** Validates and normalizes ServerRoot */
   public void validateroot() throws IOException {
      if(ServerRoot.equals("")) { System.out.println("Error: No server root defined");
                                  throw new IOException();
                                }
      if(! new File(ServerRoot).isDirectory()) { System.out.println("Error: ServerRoot directory "+ServerRoot+" does not exists");
                                   throw new IOException();
                                }
      ServerRoot = new File(ServerRoot).getCanonicalPath();
   }

   /** Main entry point for httpd.conf reading */
   public static void readapacheconfig(String s, Vector servers) throws FileNotFoundException, IOException
   {
    DataInputStream dis;

    wwwserver def=new wwwserver("default");
    def.AccessConfig="conf"+File.separator+"access.conf";
    def.ResourceConfig="conf"+File.separator+"srm.conf";

    wwwserver server;
    apache a;
    server=def;
    System.out.println("Loading Apache ServerRoot from config file: "+s);
    a = new apache(findapacheserverroot(new DataInputStream(new BufferedInputStream(new FileInputStream(s)))));
    a.validateroot();
    a.parseapacheconf(s,def,servers);
    if(!servers.contains(def)) servers.addElement(def);

    /* zpracovat vsechny pridavne konfiguracni soubory */
    znovu:
    while(true)
    {
     for(int i=0;i<servers.size();i++)
      {
       if(((wwwserver)servers.elementAt(i)).ResourceConfig!=null)
            { a.parseapacheconf(a.toAbsolute(((wwwserver)servers.elementAt(i)).ResourceConfig),
              (wwwserver)servers.elementAt(i), servers);
              ((wwwserver)servers.elementAt(i)).ResourceConfig=null;
              continue znovu;
            }

       if(((wwwserver)servers.elementAt(i)).AccessConfig!=null)
            { a.parseapacheconf(a.toAbsolute(((wwwserver)servers.elementAt(i)).AccessConfig),
              (wwwserver)servers.elementAt(i), servers);
              ((wwwserver)servers.elementAt(i)).AccessConfig=null;
              continue znovu;
            }
      } /* for */
      break;
    } /* while  */
    System.out.println("Done. "+servers.size()+" hosts defined");
   }

   private void parseapacheconf(String filename, wwwserver server,Vector servers) throws IOException
   {
    String line,token;
    StringTokenizer st;
    wwwserver tmpserver=null;

    File f=new File(filename);
    if(f.canRead()==false) return;
    DataInputStream dis=new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));
    System.out.println("Parsing apache config file: "+filename);
    while ( (line = dis.readLine()) != null)
    {
        if(line.startsWith("#")) continue;
   	  st=new StringTokenizer(line);
             if(st.hasMoreTokens()==false) continue;
             token=st.nextToken();

             if(token.equalsIgnoreCase("ServerName")) {server.setName(st.nextToken());
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("ServerAdmin")) {server.setAdmin(st.nextToken());
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("ResourceConfig")) {server.ResourceConfig=st.nextToken();
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("AccessConfig")) {server.AccessConfig=st.nextToken();
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("DocumentRoot")) {server.addalias("/",toAbsolute(st.nextToken()),false);
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("Alias")) {server.addalias(st.nextToken(),toAbsolute(st.nextToken()),false);
             					    continue;
                                                       }
             if(token.equalsIgnoreCase("ScriptAlias")) {server.addalias(st.nextToken(),toAbsolute(st.nextToken()),true);
             					    continue;
                                                       }
             /* < tag > s mezerou - hack */
             if(token.equals("<")&&st.hasMoreTokens()) token=st.nextToken();

            /* pokud zaciname na < tak zkratit */
             if(token.startsWith("<")) {
                                         token=(token.substring(1)).trim();
                                       }

             /* pokud koncime na > tak taky zkratit */
             if(token.endsWith(">")) {
                                         token=(token.substring(0,token.length()-1)).trim();
                                     }

             /* obsluha virtualnich serveru */
             if(token.equalsIgnoreCase("VirtualHost"))
               {
                if(tmpserver!=null) throw new IOException("Nested virtual hosts.");
                tmpserver=server;
                token=st.nextToken();
                /* pokud konci na > tak zkratit */
                if(token.endsWith(">")) token=(token.substring(0,token.lastIndexOf(">"))).trim();
                server=new wwwserver(token);
                servers.addElement(server);
                continue;
               }
             if(token.equalsIgnoreCase("/VirtualHost"))
               {
               server=tmpserver;
               tmpserver=null;
               continue;
               }
    }
    dis.close();
   }

   private static String findapacheserverroot(DataInputStream dis) throws IOException
   {
    String line,token;
    StringTokenizer st;
    String rc = "";

     while ( (line = dis.readLine()) != null)
     {
        if(line.startsWith("#")) continue;
   	  st=new StringTokenizer(line);
        if(st.hasMoreTokens()==false) continue;
        token=st.nextToken();

        if(token.equalsIgnoreCase("ServerRoot")) { rc=st.nextToken();}
     }
     dis.close();
     return rc;
   }

   /**
     Vrati absolutni cestu k souboru s vyuzitim server root pokud je to potreba
   */
   private String toAbsolute(String fn) throws IOException {
      File f = new File(fn);
      if (f.isAbsolute() || fn.startsWith(File.separator) )
      {
         /* vratime canonic verzi. fn vypada absolutne */
         return f.getCanonicalPath();
      }
      return new File(ServerRoot, fn).getCanonicalPath();
   }
}
