import java.io.*;

public abstract class plugin
{

 protected shtml document;

 public abstract String[] getKeywords();
 public abstract String getName();

 public void newDocument(shtml x)
 {
  document=x;
 }

 public boolean monitorhtml()
 {
  return false;
 }

 public String checkhtml(char input)
 {
  return ""+input;
 }

 public abstract int handle_command(String command, DataInputStream in,String filename) throws IOException;
}
