
/* TAGY: 

rotor file="file_to_add" ....

*/


import java.io.*;
import java.util.*;

public class News extends plugin
{
 public String getName()
 {
   return "Headline news and article generator 1.1";
 }
 
 public void newDocument(shtml x)
 {
   super.newDocument(x);
 }
 
 public String[] getKeywords()
 {
  String a[]={ "headlines","nbody","ntitle"};
  return a;
 }

 public int handle_command(String command,DataInputStream dis,String fname) throws IOException
 {
    String tag,tag_val;
    
    
    if(command.equals("nbody"))
    {
        String ifile=null;
        while(true)
        {
    	tag=document.get_tag_name(dis);    
	if(tag==null)  return 1;
        
        if(tag.equals("done")) { 
                                if(ifile==null) { document.error("No file defined!"); return -1;}
                                document.add_depend(ifile);
                                
                                DataInputStream is=new DataInputStream( 
                                                   new BufferedInputStream (
                                                   new FileInputStream( ifile ) ) );
                                
                                String line;
                                
                                document.writeToDocument("<center><br><b><font size=+1>\n");
                                
                                /* titulek */
                                line=is.readLine();
                                line=line.trim();
                                document.writeToDocument(line+"</font></b><br>\n");
                                
                                /* precteme datum */
                                
                                line=is.readLine();
                                document.writeToDocument(line+"</center><P>\n");
                                
                                while( !is.readLine().equals(""))
                                 ;
                                
                                while(true)
                                {
                                 line=is.readLine();
                                 if(line==null) break;
                                 document.writeToDocument(line+"\n");
                                }
                                 is.close();
                                 return 0;
                               }
        tag_val=document.get_tag_value(dis);
        if(tag_val == null ) return 1;
        ifile=document.find_file(fname,tag,tag_val);
        
        }
    
    }/* if nbody */
    
        if(command.equals("ntitle"))
    {
        String ifile=null;
        while(true)
        {
    	tag=document.get_tag_name(dis);    
	if(tag==null)  return 1;
        
        if(tag.equals("done")) { 

                                if(ifile==null) { document.error("No index file defined!"); return -1;}                                
                                document.add_depend(ifile);
                                
                                DataInputStream is=new DataInputStream( 
                                                   new BufferedInputStream (
                                                   new FileInputStream( ifile ) ) );
                                
                                String line;
                                /* titulek */
                                line=is.readLine();
                                line=line.trim();
                                document.writeToDocument(line);
                                is.close();
                                return 0;
                               }
        tag_val=document.get_tag_value(dis);
        if(tag_val == null ) return 1;
        ifile=document.find_file(fname,tag,tag_val);
        
        }
    
    }/* if ntitle */

    if(command.equals("headlines"))
    {
        Vector ifile=new Vector();
        int size=10;
        while(true)
        {
    	tag=document.get_tag_name(dis);    
	if(tag==null)  return 1;
        
        if(tag.equals("done")) { 
                                if(ifile.size()==0) { document.error("No index file defined!"); return -1;}
                                
                                bigloop:for(int zz=0;zz<ifile.size();zz++)
                                
                                {
                                
                                /* nacitame index */
                                
                                DataInputStream is=new DataInputStream( 
                                                   new BufferedInputStream (
                                                   new FileInputStream( (String)ifile.elementAt(zz) ) ) );
                                                   
                                document.add_depend((String)ifile.elementAt(zz));

                                String line;

                                while(true)
                                {
                                String URL, df;                               
                                line=is.readLine();
                                if(line==null) break;
                                line=line.trim();
                                if(line.equals("")) break;
                                StringTokenizer st;
                                st=new StringTokenizer(line);
                                df=st.nextToken();
                                URL=st.nextToken();
                                
                                /* zpracujeme datovy soubor */
                                df=document.find_file(fname,"virtual",df);                                
                                
                                if(df==null) { document.error("Can not find file "+df);continue;}
                                
                                DataInputStream ds=new DataInputStream( 
                                                   new BufferedInputStream (
                                                   new FileInputStream( df ) ) );
                                
                                document.add_depend(df);
                                document.writeToDocument("<P><a href=\""+URL+"\"><font size=+1>");
                                line=ds.readLine();
                                
                                document.writeToDocument(line);
                                document.writeToDocument("</font></a><br>\n");
                                
                                /* datum */
                                line=ds.readLine();
                                document.writeToDocument("("+line+")  ");
                                
                                while(true)
                                {
                                  line=ds.readLine();
                                  if(line==null) break;
                                  line=line.trim();
                                  if(line.equals("")) break;
                                  document.writeToDocument(line);
                                  document.writeToDocument("\n");
                                }
                                
                                ds.close();
                                document.writeToDocument("\n");
                                size--;
                                if(size==0) break bigloop;
                                }
                                is.close();
                                } /* for iffile */
                                return 0;
                               }
        tag_val=document.get_tag_value(dis);
        if(tag_val == null ) return 1;
        if(tag.equals("size"))
                            {
                             size=Integer.valueOf(tag_val).intValue();
                             continue;
                            }
        
        ifile.addElement(document.find_file(fname,tag,tag_val));
        
        }
    
    }/* if headlines */
    
    
   return 0;
 } 
 
}