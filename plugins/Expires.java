import java.io.*;
import java.util.*;

public class Expires extends plugin
{
 public static final String wkday[]={ "Sun","Mon","Tue","Wed","Thu","Fri","Sat" };
 public static final String month[]={ "Jan" , "Feb" , "Mar" , "Apr"
                      , "May" , "Jun" , "Jul" , "Aug"
                      , "Sep" , "Oct" , "Nov" , "Dec" };
 

 public String getName()
 {
   return "Expires generate plugin 1.0";
 }
 public String[] getKeywords()
 {
  String a[]={ "expires"};
  return a;
 }

 public int handle_command(String command,DataInputStream dis,String fname) throws IOException
 {

    String tag,s;

    Date d;
    d=new Date();
    while(true)
    {
    	tag=document.get_tag_name(dis);    
	if(tag==null)  return 1;
        if(tag.equals("delta"))
         {
          tag=document.get_tag_value(dis);
          if(tag==null) return 1;
          try{
          StringTokenizer st=new StringTokenizer(tag);
          int time=Integer.valueOf(st.nextToken()).intValue();
          s=st.nextToken();
          s=s.toLowerCase();
          long multi=1000L;
          if(s.equals("w")) multi=1000L*60*60*24*7;
          if(s.equals("d")) multi=1000L*60*60*24;          
          if(s.equals("h")) multi=1000L*60*60;          
          if(s.equals("m")) multi=1000L*60;          
          d=new Date(System.currentTimeMillis()+multi*time);          
          }
          catch (NoSuchElementException e) {}
          catch (NumberFormatException e1) {}

          continue;
         }
        if(tag.equals("done")) { document.writeToDocument("<META http-equiv=\"Expires\" content=\""+rfc1123date(d)+"\">");
                                 return 0;
                               }
                               
        document.error("unknown tag "+tag+" in directive Expire in file "+fname);
        return -1;
    }

 } 
 
 private String rfc1123date(Date d)
 {
  String res;
  res=wkday[d.getDay()]+", ";
  if(d.getDate()<10) res+="0";
  res+=d.getDate()+" ";
  res+=month[d.getMonth()]+" ";
  res+=(1900+d.getYear())+" ";

  return res+util.strftime(d,"%T")+" GMT";
 }



}