/* TAGY: 

rotor file="file_to_add" ....

*/


import java.io.*;
import java.util.*;

public class Rotor extends plugin
{
 private Vector rotor;
 
 public String getName()
 {
   return "Random include file rotator 1.0";
 }
 
 public void newDocument(shtml x)
 {
   super.newDocument(x);
 }
 
 public String[] getKeywords()
 {
  String a[]={ "rotor"};
  return a;
 }

 public int handle_command(String command,DataInputStream dis,String fname) throws IOException
 {
    rotor=new Vector();
    String tag,tag_val,fn;

    while(true)
    {
    	tag=document.get_tag_name(dis);    
	if(tag==null)  return 1;
        if(tag.equals("done")) { 
                                 if(rotor.size()==0) return 0;
                                 document.processfile((String)rotor.elementAt(
                                     (int)(  Math.random()/(1/(double)rotor.size())   )
                                                 ));
                                 return 0;
                               }
        tag_val=document.get_tag_value(dis);
        if(tag_val == null ) return 1;
        else 

            if(null!=(fn=document.find_file(fname,tag,tag_val)))
              {
               rotor.addElement(fn);
               continue;
              }
            else
             document.error("Can't find requested file "+tag_val);
          
                               
        document.error("unknown tag "+tag+" in directive Rotor in file "+fname);
        return -1;
    }

 } 
 
}